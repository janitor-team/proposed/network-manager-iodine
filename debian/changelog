network-manager-iodine (1.2.0-3.1) unstable; urgency=high

  * Non-maintainer upload.
  * debian/control: Set Vcs-* fields to use git packaging repo under
    Salsa Debian Group. They previously point to upstream development
    repo that do not contain any packaging information.
  * Apply patch from Ubuntu: (Closes: #908017)

  [ Logan Rosen ]
  * d/rules: Set --enable-more-warnings to "yes" so that it doesn't default to
    "error," which turns every warning into an error and causes an FTBFS due
    to deprecated declarations.

 -- Boyuan Yang <byang@debian.org>  Wed, 17 Nov 2021 12:37:25 -0500

network-manager-iodine (1.2.0-3) unstable; urgency=medium

  * Drop build-dep on libnm-gtk-dev (Closes: #862836)

 -- Guido Günther <agx@sigxcpu.org>  Mon, 22 Jan 2018 15:57:01 +0100

network-manager-iodine (1.2.0-2) unstable; urgency=medium

  * Run ./autogen.sh conditionally.
    This helps crossbuild but still allows us to build git snapshots. Based
    on a patch from Helmut Grohne (Closes: #871475)
  * Install /usr/lib/NetworkManager/VPN/nm-iodine-service.name
    (Closes: #862836)
  * Use libnm-dev instead of deprecated libnm-glib-dev
    Thanks to Michael Biebl (Closes: #862836)
  * Call adduser with --gecos.
    Thanks to #Ralf Treinen (Closes: #846248)
  * appdata: rename the xml file
  * Install appdata file and translations
    Thanks to Michael Biebl (Closes: #852704)
  * Build-dep on libnm-gtk-dev
  * Let dh handle any autoreconf / configure invocations
  * Bump standards version
  * Install appdata into metainfo/

 -- Guido Günther <agx@sigxcpu.org>  Mon, 22 Jan 2018 09:58:49 +0100

network-manager-iodine (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0
  * [04bb2dd] Use https URL for VCS-Git

 -- Guido Günther <agx@sigxcpu.org>  Thu, 21 Jul 2016 10:16:34 +0200

network-manager-iodine (1.1.0-1) unstable; urgency=medium

  * [903e67b] Bump build-dep on libnm-gtk-dev.
    Thanks to Logan Rosen (Closes: #771821)
  * [872589b] Update dependencies for NM 1.1
    (Closes: #817990)
  * [37e1783] Use secure URL for gitweb
  * [7296140] Bump standards version - no source changes neede
  * [df03f98] Make override_dh_makeshlibs multiarch aware

 -- Guido Günther <agx@sigxcpu.org>  Sun, 13 Mar 2016 13:28:32 +0100

network-manager-iodine (0.0.5-1) unstable; urgency=medium

  * New upstream version No new features, just compatibility with newer GNOME
    APIs. (Closes: #764839)
  * [3ff887f] Drop 0001-Disabe-gnome-keyring-deprecation-warnings.patch,
    fixed upstream
  * [95f47ae] Build-depend on libnm-gtk and libsecrets
  * [7cfa859] Bump standards version to 0.9.5

 -- Guido Günther <agx@sigxcpu.org>  Sun, 12 Oct 2014 14:15:13 +0200

network-manager-iodine (0.0.5~0.gita09ce6-2) unstable; urgency=medium

  * [5b8a7cf] Swith to debhelper 9 for multiarch so NM finds the prefs so
    again. (Closes: #760498)
  * [b7c7eeb] Disable gnome keyring deprecation warnings

 -- Guido Günther <agx@sigxcpu.org>  Thu, 04 Sep 2014 23:31:30 +0200

network-manager-iodine (0.0.5~0.gita09ce6-1) unstable; urgency=medium

  * Forward to current git (Closes: #741764)
  * [6895dd4] Drop patches appplied upstream

 -- Guido Günther <agx@sigxcpu.org>  Mon, 17 Mar 2014 20:20:24 +0100

network-manager-iodine (0.0.4-2) unstable; urgency=low

  * [3bd8d8c] Avoid deprecation warnings on newer glib/gtk. (Closes: #707469)

 -- Guido Günther <agx@sigxcpu.org>  Sat, 11 May 2013 17:06:45 +0200

network-manager-iodine (0.0.4-1) unstable; urgency=low

  * [aac1905] New upstream version 0.0.4
  * [db08d93] Remove all patches applied upstream

 -- Guido Günther <agx@sigxcpu.org>  Tue, 25 Dec 2012 13:39:39 +0100

network-manager-iodine (0.0.3-1) unstable; urgency=low

  * New upstream version
  * Upload to unstable
  * [1b0175f] Check password write result

 -- Guido Günther <agx@sigxcpu.org>  Tue, 27 Mar 2012 22:25:25 +0200

network-manager-iodine (0.0.2-1) experimental; urgency=low

  * New upstream version
  * [f0b6fa0] Update VCS fields
  * [87f56f6] Fix homepage URL (Closes: #660351)

 -- Guido Günther <agx@sigxcpu.org>  Sun, 19 Feb 2012 19:31:22 +0100

network-manager-iodine (0.0.1-1) experimental; urgency=low

  * Initial Release (Closes: #660019)

 -- Guido Günther <agx@sigxcpu.org>  Wed, 08 Feb 2012 13:33:27 +0100

